#!/usr/bin/env python3

import os
import argparse
from jinja2 import Template

parser = argparse.ArgumentParser()
parser.add_argument('NAME', help='Project name')
args = parser.parse_args()


def render(name):
    rendered = Template(open('Dockerfile.j2').read()).render(name=name)
    with open(f'Dockerfile-{name}', 'w') as f:
        f.write(rendered)


if args.NAME == 'all':
    dirs = (d for d in os.listdir('image-specific') if not d.startswith('.'))
    for p in dirs:
        render(p)
else:
    render(args.NAME)
