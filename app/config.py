import yaml
import os


_conf_path = (os.getenv('JABB_DIR') if os.getenv('JABB_DIR') else 'image-specific')+'/config.yml'


class _Empty:
    pass


class __Config:
    def __init__(self, path):
        with open(path, 'r') as f:
            self.__dict__ = yaml.safe_load(f)
            for k, v in self.__dict__.items():
                setattr(self, k, _Empty())
                setattr(getattr(self, k), '__dict__',
                        {k: (lambda x: tuple(x) if isinstance(x, list) else x)(v) for k, v in v.items()})


config = __Config(_conf_path)
