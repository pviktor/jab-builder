import os
import sys
import logging
import storage
import argparse
import handlers
import config

from telegram.ext import Updater, CommandHandler, Filters, MessageHandler, InlineQueryHandler

VARNAME = 'JABB_TOKEN'
TOKEN = os.getenv(VARNAME)


def init_log():
    logging.basicConfig(
        format='%(asctime)s %(levelname)-8s [%(filename)s:%(lineno)d] %(funcName)s: %(message)s',
        datefmt='%H:%M:%S',
        level=logging.ERROR
    )

    global log
    log = logging.getLogger(__name__)


def run_bot():
    if not TOKEN:
        log.fatal(f'{VARNAME} env variable is not provided')
        sys.exit(1)

    updater = Updater(TOKEN, use_context=True)
    dp = updater.dispatcher

    # Handle inline
    dp.add_handler(InlineQueryHandler(handlers.InlineHandler()))

    # Handle private messages
    pr_handler = handlers.PrivateMessagesHandler()
    dp.add_handler(CommandHandler('start', pr_handler.build_jab, filters=Filters.private))
    dp.add_handler(CommandHandler('build', pr_handler.build_jab, filters=Filters.private))
    dp.add_handler(CommandHandler('cancel', pr_handler.build_jab_cancel, filters=Filters.private))
    dp.add_handler(CommandHandler('help', pr_handler.help_command, filters=Filters.private))
    dp.add_handler(MessageHandler(callback=pr_handler.get_message, filters=Filters.private))

    # Start polling
    log.info('Starting polling')
    updater.start_polling()
    updater.idle()


def parse_args():
    levels = {
        'FATAL': logging.FATAL,
        'ERROR': logging.ERROR,
        'WARN': logging.WARN,
        'INFO': logging.INFO,
        'DEBUG': logging.DEBUG
    }

    parser = argparse.ArgumentParser()
    parser.add_argument('--level', help='Log level', choices=levels.keys(), default=levels['INFO'])
    args = parser.parse_args()

    storage.init(config.config.storage.path)

    # Set logging level
    logging.getLogger(__name__).setLevel(args.level)
    logging.getLogger('handlers').setLevel(args.level)

    return args


def main():
    init_log()
    parse_args()
    run_bot()


if __name__ == '__main__':
    main()
