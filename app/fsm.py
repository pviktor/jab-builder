import drawer
import storage

from transitions import Machine, State
from PIL import Image
from io import BytesIO
from config import config

resp = config.responses


class BaseBuilderFSM:
    states = [
        State(name='init'),
        State(name='text_asked', on_enter='ask_text'),
        State(name='image_asked', on_enter='ask_image'),
        State(name='result_sent', on_enter='send_result'),
        State(name='canceled', on_enter='send_cancel')
    ]

    def __init__(self, user_id):
        self.user_id = user_id
        self.text = None
        self.image = None

        self.machine = Machine(model=self, states=self.states, initial='init')
        self.machine.add_transition(trigger='start', source='init', dest='text_asked')
        self.machine.add_transition(trigger='provide_text', source='text_asked', dest='image_asked')
        self.machine.add_transition(trigger='provide_image', source='image_asked', dest='result_sent')
        self.machine.add_transition(trigger='cancel', source='text_asked', dest='canceled')
        self.machine.add_transition(trigger='cancel', source='image_asked', dest='canceled')
        self.machine.add_transition(trigger='start', source='result_sent', dest='text_asked')
        self.machine.add_transition(trigger='start', source='canceled', dest='text_asked')

    def ask_text(self):
        raise NotImplementedError

    def ask_image(self):
        raise NotImplementedError

    def send_result(self):
        raise NotImplementedError

    def send_cancel(self):
        raise NotImplementedError


class PrivateDialogFSM(BaseBuilderFSM):
    """FSM for PM with bot.

    Doesn't rely on a message_id
    """
    def __init__(self, user_id, update, context):
        super().__init__(user_id)
        self.update = update
        self.context = context

    def ask_text(self):
        self.context.bot.send_message(
            chat_id=self.update.message.chat.id,
            text=resp.select_text
        )

    def ask_image(self, **kwargs):
        # get text
        self.text = kwargs['text']

        # send message
        self.context.bot.send_message(
            chat_id=self.update.message.chat.id,
            text=resp.select_image
        )

    def send_result(self, **kwargs):
        self.image = kwargs['image']

        image = Image.open(BytesIO(self.image))
        image_final = drawer.build_image(self.text, image)

        bio = BytesIO()
        bio.name = '/tmp/image.jpeg'
        image_final.save(bio, 'JPEG')
        bio.seek(0)
        file = self.context.bot.send_photo(self.update.message.chat.id, photo=bio)
        storage.store_image(
            self.user_id,
            self.text,
            sorted(file.photo, reverse=True, key=lambda x: x['file_size'])[0]['file_id'])

    def send_cancel(self):
        self.context.bot.send_message(chat_id=self.update.message.chat.id, text=resp.canceled)


class GroupFSM(BaseBuilderFSM):
    """FSM for messages in group

    DO rely on a message_id
    Responses to messages as a Reply
    """
    ...
