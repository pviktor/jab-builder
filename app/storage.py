from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import relationship


# -- Code to enable foreign_keys in sqlite3
# https://docs.sqlalchemy.org/en/13/dialects/sqlite.html#foreign-key-support
from sqlalchemy.engine import Engine
from sqlalchemy import event


@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
    cursor = dbapi_connection.cursor()
    cursor.execute("PRAGMA foreign_keys=ON")
    cursor.close()
# --


Base = declarative_base()


def init(path):
    global engine, session
    engine = create_engine(f'sqlite:///{path}')
    session = sessionmaker(bind=engine)()
    Base.metadata.create_all(engine)


class Jab(Base):
    __tablename__ = 'jabs'

    id = Column(Integer, primary_key=True)
    text = Column(String)
    file_id = Column(String)
    user_id = Column(Integer, ForeignKey('users.id'), index=True)


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    children = relationship("Jab")


def store_image(user_id, text, file_id):
    user = User(id=user_id)
    jab = Jab(user_id=user_id, text=text, file_id=file_id)
    session.merge(user)
    session.add(jab)
    session.commit()


def get_user_jabs(user_id, search):
    result = session.query(Jab).filter_by(user_id=user_id).filter(Jab.text.ilike(f'{search}%')).limit(100).all()
    return ((jab.text, jab.file_id) for jab in result)
