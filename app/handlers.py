import fsm
import logging
import storage

from telegram import InlineQueryResultCachedPhoto, InlineQueryResultArticle, InputTextMessageContent
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from functools import partial
from itertools import count
from transitions.core import MachineError
from config import config

logging.getLogger(__name__).addHandler(logging.NullHandler())
log = logging.getLogger(__name__)
resp = config.responses


class PrivateMessagesHandler:
    def __init__(self):
        self.fsms = {}

    def build_jab(self, u, c):
        chat_id = u.message.chat.id

        try:
            machine = self.fsms[chat_id]
        except KeyError:
            machine = fsm.PrivateDialogFSM(chat_id, u, c)

        try:
            machine.start()
            self.fsms[chat_id] = machine
        except MachineError:
            c.bot.send_message(chat_id=chat_id, text=resp.operation_started)

    def build_jab_cancel(self, u, _):
        chat_id = u.message.chat.id

        try:
            machine = self.fsms[chat_id]
        except KeyError:
            return

        machine.cancel()
        del self.fsms[chat_id]

    def get_message(self, u, c):
        chat_id = u.message.chat.id
        send = partial(c.bot.send_message, chat_id=chat_id)

        if u.message.text:
            self.get_text_private(u, c)
            return
        if u.message.photo:
            self.get_image_private(u, c, u.message.photo[-1])
            return
        if u.message.document:
            self.get_image_private(u, c, u.message.document)
            return
        if u.message.sticker:
            self.get_image_private(u, c, u.message.sticker)
            return

        try:
            machine = self.fsms[chat_id]
            if machine.state == 'image_asked':
                send(text=resp.provide_img)
            elif machine.state == 'text_asked':
                send(text=resp.provide_text)
        except KeyError:
            return

    def get_text_private(self, u, c):
        chat_id = u.message.chat.id
        text = u.message.text

        log.debug(f'Received text {text}')

        send = partial(c.bot.send_message, chat_id=chat_id)

        try:
            machine = self.fsms[chat_id]
        except KeyError:
            send(text=resp.to_start)
            return

        try:
            machine.provide_text(text=text)
            self.fsms[chat_id] = machine
        except MachineError:
            if machine.state == 'image_asked':
                send(text=resp.provide_img)

    def get_image_private(self, u, c, file):
        chat_id = u.message.chat.id

        send = partial(c.bot.send_message, chat_id=chat_id)

        try:
            machine = self.fsms[chat_id]
        except KeyError:
            send(text=resp.to_start)
            return

        try:
            image = file.get_file().download_as_bytearray()
            machine.provide_image(image=image)
            self.fsms[chat_id] = machine
        except MachineError:
            if machine.state == 'text_asked':
                send(text=resp.provide_text)

    @staticmethod
    def help_command(u, c):
        c.bot.send_animation(u.message.chat.id,
                             animation=resp.help_file,
                             caption=resp.help_text)


class InlineHandler:
    def __call__(self, u, c):
        query = u.inline_query.query.strip()
        from_user = u.inline_query.from_user

        pictures = list(storage.get_user_jabs(from_user.id, query))
        c = count(0)
        if len(pictures) != 0:
            results = [
                InlineQueryResultCachedPhoto(
                    id=next(c),
                    title=jab[0],
                    photo_file_id=jab[1]
                )

                for jab in pictures
            ]
        else:
            output_text = resp.absent
            results = [
                InlineQueryResultArticle(
                    id=next(c),
                    title=resp.absent_title,
                    input_message_content=InputTextMessageContent(output_text),
                    reply_markup=InlineKeyboardMarkup.from_button(
                        InlineKeyboardButton(resp.go_to_pm, url=resp.bot_link)),
                    description=output_text)
            ]

        try:
            u.inline_query.answer(results, cache_time=1, is_personal=True)
        except Exception as ex:
            log.error(f'Cannot send: {ex}')
