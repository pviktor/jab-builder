from os import getenv
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
from config import config

ci = config.image
_base_path = (getenv('JABB_DIR') if getenv('JABB_DIR') else 'image-specific')+'/assets/'
_text_coord = ci.text_coord
_text_color = ci.text_color
_font_size = ci.font_size
_paste_pos = ci.paste_pos
_paste_size = ci.paste_size
_paste_size_ = _paste_size, _paste_size
_image = Image.open(_base_path+ci.image)
_font = ImageFont.truetype(_base_path+ci.font, _font_size)


def build_image(text, mark):
    out = _image.copy()
    mark.thumbnail(_paste_size_, Image.ANTIALIAS)

    width, height = mark.size
    actual_paste_pos = (
        _paste_pos[0] + (_paste_size_[0] - width) // 2,
        _paste_pos[1] + (_paste_size_[1] - height) // 2,
    )

    try:
        if ci.text_transform == 'upper':
            text = text.upper()
    except AttributeError:
        pass

    ImageDraw.Draw(out).text(
        _text_coord,
        text,
        _text_color,
        font=_font
    )

    try:
        out.paste(mark, actual_paste_pos, mark)
    except ValueError:
        out.paste(mark, actual_paste_pos)

    return out
