# Jab Builder 🐸

The original version is https://t.me/jab_builder_bot

## Run

```shell
python3 -m venv venv
. venv/bin/activate
pip3 install -r requirements.txt
cd app
JABB_TOKEN='your_bot_token' python3 bot.py
```

### Run in docker
At first create Dockerfile
```bash
pip3 install jinja2
./make-dockerfile.py jab
```

Then build an image and run a container:
```shell
docker build -f Dockerfile-jab -t jab-builder-bot .
docker run --name some-jab-builder -d -e JABB_TOKEN='your_token' jab-builder-bot
```

## Environment variables
- `JABB_TOKEN` — bot token
- `JABB_DIR` — path to search assets and config

## State diagram
![State diagram](state_diagram.png)